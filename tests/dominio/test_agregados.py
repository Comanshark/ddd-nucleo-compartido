"""
MÓDULO PRUEBAS DE AGREGADOS
--------------------

Este módulo contiene las pruebas de los objetos valor, entidades y agregados raíz del asaaasdf
núcleo de dominio.

Autor: Carlos E. Rivera (carlos.rivera.munoz@disr.it) (carlos.rivera.munoz@disr.it)
"""
from dataclasses import dataclass as objeto_valor
from unittest import TestCase

from nucleo_compartido.dominio.nucleo.agregados import ID, Entidad


class PruebaID(TestCase):
    """Prueba las ID genéricas"""

    def test_instancia_base(self) -> None:
        """Prueba instanciar la clase base de ID"""

        with self.assertRaises(TypeError):
            id = ID[int](1)  # pyright: ignore


class PruebaEntidad(TestCase):
    """Prueba de las entidades"""

    def test_instancia_base(self) -> None:
        """Prueba instanciar las clases base de las entidades"""

        class IntID(ID[int]):
            pass

        with self.assertRaises(TypeError):
            id = IntID(1)
            entidad = Entidad(id)  # pyright: ignore

    def test_diccionario_entidad(self) -> None:
        """Prueba la definición de diccionarios de entidades"""

        diccionario_test = {
            "id": 1,
            "nombre_nombre": "Carlos",
            "nombre_apellido": "Rivera",
        }

        @objeto_valor(frozen=True)
        class Nombre:
            nombre: str
            apellido: str

        class IntID(ID[int]):
            pass

        class Persona(Entidad[IntID]):
            __nombre: Nombre

            def __init__(self, id: IntID, nombre: str, apellido: str) -> None:
                super().__init__(id)
                self.__nombre = Nombre(nombre=nombre, apellido=apellido)

        persona: Persona = Persona(IntID(1), "Carlos", "Rivera")
        diccionario = persona.dict()

        self.assertEqual(list(diccionario.keys()),
                         list(diccionario_test.keys()))
        self.assertEqual(list(diccionario.values()),
                         list(diccionario_test.values()))
