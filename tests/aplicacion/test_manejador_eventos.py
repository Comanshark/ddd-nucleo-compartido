"""
PRUEBAS DE MANEJADOR DE EVENTOS
-------------------------------

Este paquete contiene las pruebas del manejador de eventos. También contiene las pruebas
de los casos de uso que se manejan con estos.

Autor: Carlos E. Rivera (carlos.rivera.munoz@disr.it)
"""

from dataclasses import dataclass
from unittest import IsolatedAsyncioTestCase

from nucleo_compartido.aplicacion.caso_de_uso import CasoDeUso
from nucleo_compartido.aplicacion.manejador_de_eventos import ManejadorDeEventos
from nucleo_compartido.dominio import Evento
from nucleo_compartido.dominio.nucleo.agregados import ID, AgregadoRaiz
from nucleo_compartido.dominio.nucleo.clase_datos import ClaseDatos, IClaseDatos


notificacion: str = ""


class PruebaManejadorEventos(IsolatedAsyncioTestCase):
    """Pruebas del manejador de eventos"""

    async def test_manejar_evento(self) -> None:
        """Esta prueba maneja un evento de un caso de uso a otro"""

        global notificacion

        # Creaión del manejador
        class ManejadorPersonas(ManejadorDeEventos):
            pass

        # Creación de conceptos de dominio.
        class IDPersona(ID[int]):
            pass

        @dataclass(frozen=True)
        class PersonaCreada(Evento):
            id_persona: int = 0
            nombre: str = ""

        class Persona(AgregadoRaiz[IDPersona]):
            nombre: str
            apellido: str

            def __init__(self, id: IDPersona, nombre: str, apellido: str) -> None:
                super().__init__(id)
                self.nombre = nombre
                self.apellido = apellido

            @classmethod
            def crear_persona(cls, nombre: str, apellido: str) -> "Persona":
                nuevo_id = IDPersona(1)
                nueva_persona = cls(nuevo_id, nombre, apellido)

                # Se agrega el evento de una persona creada
                nueva_persona._agregar_evento(
                    PersonaCreada(nombre=f"{nombre}", id_persona=1)
                )

                return nueva_persona

        # Creación de conceptos de aplicación
        @dataclass(frozen=True)
        class ComandoPersona(ClaseDatos):
            nombre: str
            apellido: str

        @dataclass(frozen=True)
        class ComandoNotificacion(ClaseDatos):
            nombre: str

        @dataclass(frozen=True)
        class RespuestaCreacion(ClaseDatos):
            mensaje: str

        class CrearPersona(CasoDeUso[ComandoPersona]):
            async def __call__(self) -> IClaseDatos:
                """Crea una persona en el sistema"""

                persona = Persona.crear_persona(**self.accion.dict())
                await self._notificar_eventos(persona.eventos)

                return RespuestaCreacion("Persona creada")

        class NotificarPersona(CasoDeUso[ComandoNotificacion]):
            eventos_admitidos = (PersonaCreada,)

            async def __call__(self) -> IClaseDatos:
                global notificacion
                respuesta = RespuestaCreacion(
                    mensaje=f"Persona {self.accion.nombre} notificada"
                )
                notificacion = respuesta.mensaje
                return respuesta

        # Creación de manejador y suscripción de casos de uso
        manejador = ManejadorPersonas()
        manejador.suscribir(CrearPersona)
        manejador.suscribir(NotificarPersona)

        # Creación y ejecución de caso de uso
        persona_nueva = ComandoPersona("Carlos", "Rivera")
        crear_persona = CrearPersona(persona_nueva, manejador)
        respuesta_creacion = await crear_persona()

        self.assertEqual(respuesta_creacion.dict()[
                         "mensaje"], "Persona creada")
        self.assertEqual(notificacion, "Persona Carlos notificada")

        # Desuscribir un caso de uso
        notificacion = ""
        manejador.desuscribir(NotificarPersona)
        crear_persona2 = CrearPersona(persona_nueva, manejador)
        await crear_persona2()

        self.assertEqual(notificacion, "")
