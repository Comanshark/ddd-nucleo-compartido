"""
EXCEPCIONES DE APLICACIÓN
-------------------------

Este módulo contiene las excepciones que se pueden dar en la ejecución de un caso de uso.

Autor: Carlos E. Rivera
"""

from dataclasses import dataclass


@dataclass(frozen=True)
class ExcepcionDeAplicacion(Exception):
    """Excepción de Aplicación"""

    mensaje: str

    def dict(self) -> dict[str, str]:
        """Retorna un diccionario de una excepción"""

        return {
            'error': type(self).__name__,
            'mensaje': self.mensaje
        }


@dataclass(frozen=True)
class EventoNoAdmitido(ExcepcionDeAplicacion):
    """Evento no admitido en un caso de uso"""
    ...
