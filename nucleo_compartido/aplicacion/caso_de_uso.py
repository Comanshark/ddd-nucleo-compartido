"""
MÓDULO DE CASO DE USO
---------------------

Este módulo contiene las interfaces para desarrollar casos de uso.

Autor: Carlos E. Rivera (carlos.rivera.munoz@disr.it)
"""

from abc import abstractmethod
from asyncio import Queue
from typing import Generic, Iterable, Self, TypeVar, cast
from nucleo_compartido.aplicacion.excepciones import EventoNoAdmitido

from nucleo_compartido.dominio import Evento
from nucleo_compartido.aplicacion.interfaces import (
    INotificador,
    ISuscriptor,
)
from nucleo_compartido.dominio.nucleo.clase_datos import IClaseDatos
from nucleo_compartido.dominio.nucleo.eventos import Evento


T = TypeVar("T", bound=IClaseDatos)


class CasoDeUso(ISuscriptor, Generic[T]):
    """Caso de Uso"""

    __accion: T
    __notificador: INotificador
    eventos_admitidos: Iterable[type[Evento]] = tuple()

    @property
    def accion(self) -> T:
        return self.__accion

    def __init__(self, accion: T, notificador: INotificador) -> None:
        """Inicializa el caso de uso"""

        self.__accion = accion
        self.__notificador = notificador
        self.eventos_admitidos = self.eventos_admitidos

    @classmethod
    def por_evento(cls, evento: Evento, notificador: INotificador) -> Self:
        if type(evento) not in cls.eventos_admitidos:
            raise EventoNoAdmitido(
                f"El evento {evento.nombre} no es admitido en {cls.__name__}"
            )

        return cls(cast(T, evento), notificador)

    async def _notificar_eventos(self, eventos: Queue[Evento]) -> None:
        """Notifica un evento"""

        while not eventos.empty():
            await self.__notificador.notificar(await eventos.get())

    @abstractmethod
    async def __call__(self) -> IClaseDatos:
        """Ejecuta el caso de uso"""

        ...
