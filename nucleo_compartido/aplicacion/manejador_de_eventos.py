"""
MÓDULO DE MANEJADOR DE EVENTOS
------------------------------

Este módulo contiene la base para crear manejadoras de eventos.

Autor: Carlos E. Rivera (carlos.rivera.munoz@disr.it)
"""
from typing import Type

from nucleo_compartido.aplicacion.interfaces import (
    INotificador,
    ISuscriptor,
)
from nucleo_compartido.dominio.nucleo.eventos import Evento


class ManejadorDeEventos(INotificador):
    """Manejador de Eventos"""

    __suscriptores: list[Type[ISuscriptor]]

    def __init__(self) -> None:
        """Inicializa el manejador de eventos"""

        self.__suscriptores = []

    def suscribir(self, suscriptor: Type[ISuscriptor]) -> None:
        self.__suscriptores.append(suscriptor)

    def desuscribir(self, suscriptor: Type[ISuscriptor]) -> None:
        self.__suscriptores.remove(suscriptor)

    async def notificar(self, evento: Evento) -> None:
        for suscriptor in self.__suscriptores:
            try:
                ejecutar = suscriptor.por_evento(evento, self)
                await ejecutar()
            except Exception as excepcion:
                pass  # TODO: Agregar logging para notificar el error.
