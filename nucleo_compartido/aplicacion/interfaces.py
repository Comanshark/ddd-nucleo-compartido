"""
MÓDULO DE INTERFACES DE APLICACIÓN
----------------------------------

Este módulo contiene todas las interfaces de aplicación.

Autor: Carlos E. Rivera (carlos.rivera@disr.it)
"""

from typing import Protocol, Self, Type

from nucleo_compartido.dominio.nucleo.clase_datos import IClaseDatos
from nucleo_compartido.dominio.nucleo.eventos import Evento


class ISuscriptor(Protocol):
    """Interfaz de suscriptor"""

    async def __call__(self) -> IClaseDatos:
        """Ejecuta el suscriptor"""

        ...

    @classmethod
    def por_evento(cls, evento: Evento, notificador: "INotificador") -> Self:
        """Inicializa un suscriptor por un evento"""

        ...


class INotificador(Protocol):
    """Interfaz de manejador de eventos"""

    def suscribir(self, suscriptor: Type[ISuscriptor]) -> None:
        """Suscribe un suscriptor"""

        ...

    def desuscribir(self, suscriptor: Type[ISuscriptor]) -> None:
        """Desuscribe un suscriptor"""

        ...

    async def notificar(self, evento: Evento) -> None:
        """Notifica a los suscriptores"""

        ...

    async def ejecutar(self, accion: IClaseDatos) -> IClaseDatos:
        """Ejecuta un suscriptor en base a una acción"""

        ...
