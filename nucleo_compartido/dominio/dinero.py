"""
MÓDULO DE DINERO
----------------

Este módulo contiene la lógica de trabajar con dinero.

Autor: Carlos E. Rivera (carlos.rivera.munoz@disr.it)
"""
from decimal import Decimal, ROUND_HALF_UP
from typing import Union


class Dinero:

    __valor: Decimal

    def __init__(self, valor: Decimal | float | int) -> None:
        """Inicializa una moneda"""

        self.__valor = Decimal(valor).quantize(Decimal('0.00'), rounding=ROUND_HALF_UP)
    
    @property
    def valor(self) -> float:
        """Retorna el valor de la moneda"""

        return float(self.__valor)
    
    def __add__(self, otro: Union['Dinero', Decimal, float, int]) -> 'Dinero':
        """Operación suma entre monedas"""

        if isinstance(otro, Dinero):
            return Dinero(self.__valor + otro.__valor)
        else:
            return Dinero(self.__valor + Decimal(otro))

    def __sub__(self, otro: Union['Dinero', Decimal, float, int]) -> 'Dinero':
        """Operación resta entre monedas"""

        if isinstance(otro, Dinero):
            return Dinero(self.__valor - otro.__valor)
        else:
            return Dinero(self.__valor - Decimal(otro))

    def __mul__(self, otro: Union['Dinero', Decimal, float, int]) -> 'Dinero':
        """Operación multiplicación entre monedas"""

        if isinstance(otro, Dinero):
            return Dinero(self.__valor * otro.__valor)
        else:
            return Dinero(self.__valor * Decimal(otro))

    def __truediv__(self, otro: Union['Dinero', Decimal, float, int]) -> 'Dinero':
        """Operación división entre moneadas"""

        if isinstance(otro, Dinero):
            return Dinero(self.__valor / otro.__valor)
        else:
            return Dinero(self.__valor / Decimal(otro))
