"""
PAQUETE DE DOMINIO
------------------

Este paquete contiene los elementos básicos de dominio, así como objetos valor que son
utilizados.

Autor: Carlos E. Rivera (carlos.rivera.munoz@disr.it)
"""

# IMPORTACIÓN DE NÚCLEO
from nucleo_compartido.dominio.nucleo import *

# IMPORTACIÓN DE CLASES UTILITARIAS
from nucleo_compartido.dominio.dinero import Dinero
