"""
MÓDULO DE CLASE DE DATOS
------------------------

Este módulo contiene las clase de datos (dataclass) inmutables para tranferencia
de los mismos en diferentes componentes de los programas. Tiene como objetivo mantener
una comunicación común entre los distintos componentes de los sistemas.

Autor: Carlos E. Rivera (carlos.rivera.munoz@disr.it)
"""

from abc import ABC
from dataclasses import asdict, dataclass
from typing import Any, Protocol, Self, cast


class IClaseDatos(Protocol):
    """Interfaz de clase de datos"""

    @classmethod
    def por_dict(cls, diccionario: dict[str, Any]) -> Self:
        """Crea una clase de datos por diccionario"""
        ...

    def dict(self) -> dict[str, Any]:
        """Convierte una clase de datos en diccionario"""
        ...


@dataclass(frozen=True)
class ClaseDatos(ABC, IClaseDatos):
    """Clase Base para la comunicación entre datos"""

    @classmethod
    def por_dict(cls, diccionario: dict[str, Any]) -> Self:
        atributos = {
            x: y
            for x, y in diccionario.items()
            if x in cast(dict[str, Any], cls.__dict__["__annotations__"]).keys()
        }

        return cls(**atributos)

    def dict(self) -> dict[str, Any]:
        return asdict(self)
