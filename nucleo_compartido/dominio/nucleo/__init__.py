"""
PAQUETE DE ELEMENTOS BASE
-------------------------

Este paquete contiene los elementos base para la creación de elementos de dominio.

Autor: Carlos E. Rivera (carlos.rivera.munoz@disr.it)
"""

__all__ = [
    "ID",
    "Entidad",
    "AgregadoRaiz",
    "Evento",
    "Evento",
    "EventoIntegracion",
    "Excepcion",
]

# IMPORTACIÓN DE AGREGADOS
from nucleo_compartido.dominio.nucleo.agregados import ID, Entidad, AgregadoRaiz

# IMPORTACIÓN DE EVENTOS
from nucleo_compartido.dominio.nucleo.eventos import (
    Evento,
    Evento,
    EventoIntegracion,
)

# IMPORTACIÓN DE EXCEPCIONES
from nucleo_compartido.dominio.nucleo.excepciones import Excepcion

# IMPORTACIÓN DE REPOSITORIOS
from nucleo_compartido.dominio.nucleo.repositorios import (
    IRepositorioLectura,
    IRepositorioAgregar,
    IRepositorioActualizar,
    IRepositorioEliminar,
    IRepositorioElminarVarios,
)
