"""
MÓDULO DE AGREGADOS
-------------------

Este módulo contiene la lógica de los agregados de dominio.

Autor: Carlos E. Rivera (carlos.rivera.munoz@disr.it)
"""
from abc import ABC
from asyncio import Queue
from dataclasses import asdict, is_dataclass
from typing import TypeVar, Generic, Any

from nucleo_compartido.dominio.nucleo.eventos import Evento


TI = TypeVar("TI")  # Genérico para los ID


class ID(Generic[TI]):
    """ID de una entidad"""

    __id: TI

    @property
    def id(self) -> TI:
        """Obtiene el ID"""

        return self.__id

    def __init__(self, id: TI) -> None:
        """Inicializa un ID"""

        if type(self) is ID:
            raise TypeError("No se puede instanciar la clase base ID")

        self.__id = id

    def __eq__(self, other: Any) -> bool:
        """Iguala dos ID"""

        if not isinstance(other, type(self)):
            return NotImplemented

        return self.__id == other.__id


T = TypeVar("T", bound=ID[Any])


class Entidad(Generic[T], ABC):
    """Entidad en DDD"""

    __id: T

    @property
    def id(self) -> T:
        """Devuelve el ID de la entidad"""

        return self.__id

    def __init__(self, id: T) -> None:
        """Inicializa una entidad"""

        if type(self) is Entidad:
            raise TypeError("No se puede instanciar la clase base Entidad")

        self.__id = id

    def __eq__(self, valor: object) -> bool:
        """Iguala una entidad con otra"""

        if not isinstance(valor, type(self)):
            return NotImplemented

        return self.__id == valor.__id

    def dict(self) -> dict[str, Any]:
        """Convierte la entidad en un diccionario"""

        import re

        patron = r"_[^_]+__"

        diccionario: dict[str, Any] = {}

        for llave, valor in self.__dict__.items():
            nombre = re.sub(patron, "", llave)
            if is_dataclass(valor):
                objeto = asdict(valor)
                if len(objeto.values()) > 1:
                    for subllave, subvalor in objeto.items():
                        diccionario[f"{nombre}_{subllave}"] = subvalor
                else:
                    diccionario[nombre] = list(objeto.values())[0]
            elif issubclass(type(valor), ID):
                diccionario["id"] = valor.id
            else:
                diccionario[nombre] = valor

        return diccionario


class AgregadoRaiz(Entidad[T]):
    """Un agregado raíz en DDD"""

    __eventos: Queue[Evento] = Queue()


    @property
    def eventos(self) -> Queue[Evento]:
        return self.__eventos

    def __init__(self, id: T) -> None:
        if type(self) is AgregadoRaiz:
            raise TypeError("No se puede instanciar la clase AgregadoRaiz")

        super().__init__(id)

    def _agregar_evento(self, evento: Evento) -> None:
        """Agrega un evento a la entidad"""

        self.__eventos.put_nowait(evento)
