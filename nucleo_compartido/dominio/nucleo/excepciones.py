"""
MÓDULO DE EXCEPCIONES
---------------------

Este módulo contiene las excepciones de dominio. Estas excepciones funcionan para poder
diferenciar los errores del sistema con los errores de la lógica de negocio.

Autor: Carlos E. Rivera (carlos.rivera.munoz@disr.it)
"""
from dataclasses import dataclass, asdict


@dataclass(frozen=True)
class Excepcion(Exception):
    """Excepción de dominio"""

    mensaje: str
    alcance: str | None

    def dict(self) -> dict['str', 'str']:
        """Convierte la excepción a diccionario"""

        return asdict(self)