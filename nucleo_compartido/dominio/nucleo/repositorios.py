"""
MÓDULO DE REPOSITORIOS
----------------------

Este módulo contiene la lógica de los repositorios.

Autor: Carlos E. Rivera (carlos.rivera.munoz@disr.it)
"""
from typing import Any, Iterable, Protocol, TypeVar

from nucleo_compartido.dominio.nucleo.agregados import ID, AgregadoRaiz


TV = TypeVar("TV", bound=AgregadoRaiz, contravariant=True)  # type: ignore
TC = TypeVar("TC", bound=AgregadoRaiz, covariant=True)  # type: ignore


class IRepositorioLectura(Protocol[TC]):
    """Interfaz de repositorio de lectura"""

    def obtener_por_id(self, id: ID[Any]) -> TC | None:
        """Obtiene el agregado por el ID"""
        ...


class IRepositorioAgregar(Protocol[TV]):
    """Interfaz de repositorio para agregar"""

    async def agregar(self, agregado: TV) -> None:
        """Guarda el agregado en el repositorio"""

        ...


class IRepositorioActualizar(Protocol[TV]):
    """Interfaz de repositorio para actualizar"""

    async def actualizar(self, agregado: TV) -> None:
        """Actualiza el agregado en el repositorio"""

        ...


class IRepositorioEliminar(Protocol[TV]):
    """Interfaz de repositorio para eliminar"""

    async def eliminar(self, agregado: TV) -> None:
        """Elimina el agregado del repositorio"""

        ...


class IRepositorioElminarVarios(Protocol[TV]):
    """Interfaz de repositorio para eliminar varios"""

    async def eliminar_varios(self, agregados: Iterable[TV]) -> None:
        """Eliimina varios agregados del repositorio"""

        ...
