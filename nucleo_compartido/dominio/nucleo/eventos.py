"""
MÓDULO DE EVENTOS DE DOMINIO
----------------------------

Este módulo contiene los eventos de dominio.

Autor: Carlos E. Rivera (carlos.rivera.munoz@disr.it)
"""
from dataclasses import asdict, dataclass, field
from datetime import datetime
from typing import Any, cast
from uuid import UUID, uuid4

from nucleo_compartido.dominio.nucleo.clase_datos import ClaseDatos


@dataclass(frozen=True)
class Evento(ClaseDatos):
    """Evento base"""

    fecha: datetime = field(default_factory=datetime.now)

    @property
    def nombre(self) -> str:
        return type(self).__name__

    def __post_init__(self) -> None:
        """Validaciones después de la iniciación"""

        if type(self) is Evento:
            raise TypeError("No se puede instanciar la clase base EventoBase")

    def dict(self) -> dict[str, Any]:
        """Convierte el evento en un diccionario"""

        return {
            "nombre": self.nombre,
            "fecha": self.fecha,
            "datos": {
                llave: valor
                for llave, valor in asdict(self).items()
                if llave in ("nombre", "fecha",)
            },
        }


@dataclass(frozen=True)
class EventoIntegracion(Evento):
    """Evento de integración"""

    id: UUID = field(default_factory=uuid4)

    def __post_init__(self) -> None:
        if type(self) is EventoIntegracion:
            raise TypeError("No se puede instanciar la clase base EventoIntegracion")

    def dict(self) -> dict[str, Any]:
        resultado = super().dict()
        id = cast(dict[str, Any], resultado["datos"]).pop("id")
        resultado["id"] = id

        return resultado
