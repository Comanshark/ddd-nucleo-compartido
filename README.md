DDD NÚCLEO COMPARTIDO
=====================

Este proyecto contiene los elementos básicos para la implementación de proyectos
diseñados con **Desarrollo Orientado al Dominio** (DDD en inglés). La principal
iniciativa con este proyecto es hacer un desarrollo en DDD y pythonesco.

El proyecto se divide en tres submódulos:

- **Dominio**: con los elementos básicos para trabajar con agregados, objetos valor,
  repositorios, etc..
- **Infraestructura**: con implementaciones para trabajar con diferentes frameworks.
- **Aplicación**: con elementos para trabajar con casos de uso y desarrollo orientado a
  eventos.