# CHANGELOG



## v0.2.0 (2023-09-19)

### Feat

- **aplicacion**: añadiendo módulo de aplicación
- **dominio**: reordenando módulo de dominio

## v0.1.1 (2023-08-13)

### Refactor

- **dominio**: cambiando funcionalidad de dict en Entidades

## v0.1.0 (2023-08-13)

### Documentation

* docs: :memo: añadiendo documentación adicional

Se añade README y Docstrings actualizados. ([`5c756ff`](https://github.com/Comanshark/ddd-nucleo-compartido/commit/5c756ff5fe7cf4e1fc5cb707a8d5478b51e754ed))

### Feature

* feat: :sparkles: inicializando repositorio

Se inicializa el repositorio del proyecto

BREAK CHANGE ([`f1704e8`](https://github.com/Comanshark/ddd-nucleo-compartido/commit/f1704e8720cb4c19a3f310f30405b5e5a0c9c6ce))

### Unknown

* doc: :memo: añadiendo licencia GNU Affero GPL v3 ([`fcc91df`](https://github.com/Comanshark/ddd-nucleo-compartido/commit/fcc91df7d18a767867067d01fe2fc1305c4e3ec9))
